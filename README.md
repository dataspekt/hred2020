# Excess mortality in Croatia 2000-2019 / 2020.

Estimating expected number of deaths based on deaths-by-week data from Croatian bureau of statistics ([via Eurostat](https://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=demo_r_mwk_ts)) and plotting provisional data from year 2020 over expected number of deaths.

Recent mortality data should be downloaded manually and saved in `data/`. Model fit is also included and there is no need to refit model to update figures (just run `make_figures.R`).
