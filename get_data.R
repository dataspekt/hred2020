library(tidyverse)
library(tsibble)
library(ISOweek)

# Dataset "demo_r_mwk_ts" from Eurostat
# URL: https://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=demo_r_mwk_ts

path <-
    list.files("data", pattern = "^demo_r_mwk_ts_linear", full.names = TRUE) %>% last()

data_eurostat <-
    read_csv(path, na = c("",":")) %>%
    filter(geo %in% "HR" & sex %in% "T" ) %>%
    separate(TIME_PERIOD, into = c("year", "week"), sep = "-W") %>%
    filter(!is.na(OBS_VALUE)) %>%
    mutate(
        date = ISOweek2date(paste(year, paste0("W", week), "1", sep = "-")),
        yearweek = yearweek(date),
        doy = as.numeric(strftime(date,"%j")),
        deaths = as.numeric(OBS_VALUE),
        flags = if_else(OBS_FLAG %in% "p", "provisional", "final")
    ) %>%
    arrange(date) %>%
    select(year, week, yearweek, date, doy, flags, deaths)

# (save as "data_eurostat.csv")
write_csv(data_eurostat, "data/data_eurostat.csv")


# Dataset of COVID-19 deaths (from JHU/OWID)

tempfile <- tempfile()

download.file("https://github.com/owid/covid-19-data/raw/master/public/data/owid-covid-data.csv", tempfile)


data_owid <-
    read_csv(tempfile) %>%
    filter(location %in% "Croatia") %>%
    transmute(
        date = date,
        yearweek = yearweek(date),
        deaths = total_deaths
    ) %>%
    mutate(
        deaths_new = deaths - lag(deaths),
        deaths_new = if_else(is.na(deaths_new), deaths, deaths_new)
    ) %>%
    select(-deaths) %>%
    drop_na() %>%
    arrange(date) %>%
    filter(cumsum(deaths_new) > 0) %>%
    group_by(yearweek) %>%
    summarize(
        deaths_c19 = sum(deaths_new),
        n_weekdays = n()
    ) %>%
    mutate(
        date = as.Date(yearweek),
        doy = as.numeric(strftime(date,"%j"))
    ) %>% 
    filter(n_weekdays == 7) %>%
    select(yearweek, date, doy, deaths_c19)

# (save as "data_owid.csv")
write_csv(data_owid, "data/data_owid.csv")
